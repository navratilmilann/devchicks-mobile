import React, {Component} from 'react';
import {View} from 'react-native';
/* PACKAGES */
import * as eva from '@eva-design/eva';
import {ApplicationProvider, Text, Input, Button} from '@ui-kitten/components';
import Styles from '../styles/Main';

class Home extends Component {

	render() {
		return (
			<>
				<ApplicationProvider {...eva} theme={eva.light}>
					<View style={Styles.home_view}>
						<Text style={Styles.home_title} category='h2'>WEBRATING</Text>
						<Text style={Styles.home_subtitle} category='s1'>Rate your site!</Text>
						<Input style={Styles.home_input} placeholder='Place your Text'/>
						<Button style={Styles.home_button}>
							<Text category='s1'>Rate</Text>
						</Button>
					</View>
				</ApplicationProvider>
			</>
		);
	}
}

export default Home;
