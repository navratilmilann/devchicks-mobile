import React, {Component} from 'react';
import axios from 'axios';
import {ApplicationProvider, Avatar, Card, Text} from '@ui-kitten/components';
import * as eva from '@eva-design/eva';
import Styles from '../styles/Main';
import {ScrollView, View, Image} from 'react-native';
import {strip_html} from '../helpers';

/* PACKAGES */


class Detail extends Component {

	state = {
		site_id: null,
		site: [],
		comments: [],
	};

	constructor(props) {
		super(props);
		this.navigation = props.navigation;
		this.site_id = props.route.params.id;
		this.slug = props.route.params.slug;
	}

	componentDidMount() {

		axios.get(`http://dev.devchicks.com/api/site/${this.site_id}/${this.slug}`).then((response) => {
			const site = response.data;
			this.setState({
				site: site,
			});
		});

		axios.get(`http://dev.devchicks.com/api/site/${this.site_id}/comment`).then((response) => {
			const comments = response.data;
			this.setState({
				comments: comments,
			});
		});
	}

	render() {
		return (
			<>
				<ApplicationProvider {...eva} theme={eva.light}>
					<View style={Styles.view}>
						<ScrollView>

							<View style={[Styles.view, Styles.detail_view]}>
								<Text style={Styles.bold_text}>Name</Text>
								<Text>{this.state.site.name}</Text>
								<Text style={[Styles.bold_text, Styles.mt10]}>Description</Text>
								<Text>{strip_html(this.state.site.description)}</Text>
							</View>
							<View style={[Styles.view, Styles.mt10]}>
								{this.state.comments.map((comment) => (

									<Card
										key={comment.id}
									>
										<View
											style={[Styles.container, Styles.comment]}
										>
											<View style={Styles.comment_avatar_view}>
												<Avatar
													style={Styles.small_avatar}
													source={{uri: comment.user.profile_photo_url}}
												/>
											</View>
											<View style={Styles.comment_data_view}>
												<Text>{comment.user.name}</Text>
												<Text>{strip_html(comment.text)}</Text>
											</View>
										</View>

										<View
											style={Styles.reply_container}
										>
											{comment.replies.map((reply) => (
												<View
													key={reply.id}
													style={[Styles.container, Styles.view]}
												>
													<View
														style={[Styles.comment_avatar_view, Styles.reply_avatar_view]}>
														<Avatar
															style={Styles.small_avatar}
															source={{uri: reply.user.profile_photo_url}}
														/>
													</View>
													<View style={[Styles.comment_data_view, Styles.reply_data_view]}>
														<Text>{reply.user.name}</Text>
														<Text>{strip_html(reply.text)}</Text>
													</View>
												</View>
											))}
										</View>
									</Card>

								))}
							</View>

						</ScrollView>
					</View>
				</ApplicationProvider>
			</>
		);
	}
}

export default Detail;
