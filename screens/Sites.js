import React, {Component} from 'react';
// packages
import * as eva from '@eva-design/eva';
import axios from 'axios';
import {ApplicationProvider, Text, Card, Avatar} from '@ui-kitten/components';
import {ScrollView, View} from 'react-native';
// routes
import routes from '../routes';
// styles
import Styles from '../styles/Main';
// helpers
import {strip_html, excerpt} from '../helpers';
// screens
import Detail from './Detail';

class Sites extends Component {

	state = {
		sites: [],
	};

	constructor(props) {
		super(props);
		this.navigation = props.navigation;
	}

	componentDidMount() {
		axios.get(routes.get_sites).then((response) => {
			const sites = response.data;
			this.setState({sites});
		});
	}

	navigate(screen, props = {}) {
		return this.navigation.navigate(screen, props);
	}

	render() {
		return (
			<>
				<ApplicationProvider {...eva} theme={eva.light}>
					<View style={Styles.view}>
						<ScrollView>

							{this.state.sites.map((site) => (

								<Card style={Styles.card} key={site.id} status="primary">
									<View style={Styles.container}>
										<View style={Styles.avatar_view}>
											<Avatar
												size="giant"
												source={require('../assets/img/zoom_chick.png')}
											/>
										</View>
										<View style={Styles.data_view}>
											<Text style={[Styles.bold_text,Styles.subtitle]}>{site.name}</Text>
											<Text style={[Styles.mt10]}>
												{excerpt(site.description, 100)}
											</Text>
											<Text category="s1" style={[Styles.show_more,Styles.mt10]}
												  onPress={
													  () => this.navigate('Detail', {
														  id: site.id,
														  slug: site.slug,
													  })
												  }
											>
												Show more
											</Text>
										</View>
									</View>
								</Card>

							))}

						</ScrollView>
					</View>
				</ApplicationProvider>
			</>
		);
	}
}

export default Sites;
