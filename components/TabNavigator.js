import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {HomeStackNavigator, SiteStackNavigator} from './StackNavigator';
import Ionicons from 'react-native-vector-icons/Ionicons';

const Tab = createBottomTabNavigator();

const TabNavigator = () => {
	return (
		<Tab.Navigator
			screenOptions={({route}) => ({
				tabBarIcon: ({focused, color, size}) => {
					let iconName;

					if (route.name === 'Home') {
						iconName = 'home-outline';
					} else if (route.name === 'Sites') {
						iconName = 'globe-outline';
					}

					return <Ionicons name={iconName} size={size} color={color}/>;
				},
			})}
			tabBarOptions={{
				activeTintColor: '#3578fa',
				inactiveTintColor: 'gray',
			}}
		>
			<Tab.Screen name="Home" component={HomeStackNavigator}/>
			<Tab.Screen name="Sites" component={SiteStackNavigator}/>
		</Tab.Navigator>
	);
};

export default TabNavigator;
