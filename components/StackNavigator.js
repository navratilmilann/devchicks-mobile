import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import Home from "../screens/Home";
import Sites from "../screens/Sites";
import Detail from "../screens/Detail";

const Stack = createStackNavigator();

const HomeStackNavigator = () => {
	return (
		<Stack.Navigator>
			<Stack.Screen name="Home" component={Home} />
		</Stack.Navigator>
	);
}

const SiteStackNavigator = () => {
	return (
		<Stack.Navigator>
			<Stack.Screen name="Sites" component={Sites} />
			<Stack.Screen name="Detail" component={Detail} />
		</Stack.Navigator>
	);
}

export { HomeStackNavigator, SiteStackNavigator };
