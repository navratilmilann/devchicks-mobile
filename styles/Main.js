import {StyleSheet} from 'react-native';

const Styles = StyleSheet.create({

	// General
	bold_text: {
		fontWeight: 'bold',
	},
	show_more: {
		color: '#3578fa',
	},
	container: {
		flexDirection: 'row',
	},
	subtitle: {
		fontSize: 16,
	},
	mt10: {
		marginTop: 10,
	},

	// Home screen
	home_view: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	home_title: {
		textAlign: 'center',
		fontSize: 40,
	},
	home_subtitle: {
		textAlign: 'center',
		fontSize: 25,
		marginBottom: 20,
	},
	home_input: {
		width: '80%',
	},
	home_button: {
		marginTop: 10,
		backgroundColor: '#FFC107',
		borderColor: '#FFC107',
		width: '80%',
	},

	// Site and Detail screens
	view: {
		flex: 1,
		paddingLeft: 10,
		paddingRight: 10,
		paddingTop: 10,
	},
	card: {
		width: '100%',
		marginBottom: 10,
	},
	avatar_view: {
		width: '35%',
		justifyContent: 'center',
		alignItems: 'center',
	},
	data_view: {
		width: '65%',
	},

	// Detail screen
	detail_view: {
		paddingLeft: 20,
		paddingRight: 20,
	},
	comment: {
		paddingBottom: 10,
	},
	comment_avatar_view: {
		justifyContent: 'center',
		alignItems: 'center',
		width: '20%',
	},
	comment_data_view: {
		width: '80%',
		paddingLeft: 15,
	},
	reply_avatar_view: {
		marginLeft: '10%',
		width: '15%',
		paddingRight: 30,
	},
	reply_data_view: {
		width: '75%',
		borderLeftColor: '#ededed',
		borderLeftWidth: 4,
		paddingLeft: 20,
	},
	small_avatar: {
		width: 50,
		height: 50,
	},

	// comment_reply: {},
	// reply_container: {
	// 	width: '95%',
	// 	marginRight: 0,
	// 	marginLeft: 'auto'
	// }
});

export default Styles;
