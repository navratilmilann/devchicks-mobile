function strip_html(text) {
	if (text) {
		return text.replace(/(<([^>]+)>)/ig, '');
	} else {
		return '';
	}
}

function excerpt(str,limit) {
	if (str) {
		return str.length < limit
			? strip_html(`${str}`)
			: strip_html(`${str.substring(0, limit)}...`);
	} else {
		return '';
	}
}

export {
	strip_html,
	excerpt,
};
